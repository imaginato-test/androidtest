package com.nectarbits.test.injection

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import androidx.appcompat.app.AppCompatActivity
import com.nectarbits.test.model.database.AppDatabase
import com.nectarbits.test.ui.login.LoginViewModel

class ViewModelFactory(private val activity: AppCompatActivity): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            val db = Room.databaseBuilder(activity.applicationContext, AppDatabase::class.java, "imginato").build()
            @Suppress("UNCHECKED_CAST")
            return LoginViewModel(db.userDao()) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")

    }
}