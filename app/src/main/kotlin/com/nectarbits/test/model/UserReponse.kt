package com.nectarbits.test.model

import androidx.room.*


/**
 * Class which provides a model for UserReponse
 * @constructor Sets all properties of the UserReponse
 */
@Entity
data class UserReponse(
        @field:PrimaryKey(autoGenerate = true)
        var id: Int,
        @Embedded
        var user: User?,
        @Ignore
        val errorCode: String?,
        @Ignore
        val errorMessage: String?
){
        constructor():this(id =0, user =null, errorCode ="", errorMessage ="") }

