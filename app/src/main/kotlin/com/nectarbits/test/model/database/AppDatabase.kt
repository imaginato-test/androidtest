package com.nectarbits.test.model.database

import androidx.room.Database
import androidx.room.RoomDatabase

import com.nectarbits.test.model.UserDao
import com.nectarbits.test.model.UserReponse

@Database(entities = [UserReponse::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
}