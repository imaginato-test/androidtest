package com.nectarbits.test.model

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query



@Dao
interface UserDao {
  /*  @get:Query("SELECT * FROM userreponse")
    val all: List<UserReponse>*/

    @Insert
    fun insertAll(vararg users: UserReponse)
}