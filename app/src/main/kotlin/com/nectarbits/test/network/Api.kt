package com.nectarbits.test.network


import com.nectarbits.test.model.UserReponse
import retrofit2.Call
import retrofit2.http.*

/**
 * The interface which provides methods to get result of webservices
 */
interface Api {
    /**
     * Get Login
     */
    @FormUrlEncoded
    @POST("login")
    fun Login(@HeaderMap header: Map<String, String>, @FieldMap perameters: Map<String, String>): Call<UserReponse>
}