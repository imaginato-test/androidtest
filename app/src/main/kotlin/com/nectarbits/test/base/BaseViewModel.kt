package com.nectarbits.test.base

import androidx.lifecycle.ViewModel
import com.nectarbits.test.injection.component.DaggerViewModelInjector
import com.nectarbits.test.injection.component.ViewModelInjector
import com.nectarbits.test.injection.module.NetworkModule
import com.nectarbits.test.ui.login.LoginViewModel

abstract class BaseViewModel:ViewModel(){
    private val injector: ViewModelInjector = DaggerViewModelInjector
            .builder()
            .networkModule(NetworkModule)
            .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is LoginViewModel -> injector.inject(this)

        }
    }
}