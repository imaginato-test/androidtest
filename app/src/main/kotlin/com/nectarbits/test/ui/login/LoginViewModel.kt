package com.nectarbits.test.ui.login

import android.content.Context
import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.nectarbits.test.R
import com.nectarbits.test.base.BaseViewModel
import com.nectarbits.test.model.UserDao
import com.nectarbits.test.model.UserReponse
import com.nectarbits.test.network.Api
import io.reactivex.Completable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject


class LoginViewModel(private val userDao: UserDao): BaseViewModel(), Callback<UserReponse> {
    @Inject
    lateinit var api: Api

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val mainviewVisibility: MutableLiveData<Int> = MutableLiveData()

    val errorMessage:MutableLiveData<String> = MutableLiveData()
    val successMessage:MutableLiveData<String> = MutableLiveData()
    val errorClickListener = View.OnClickListener { login() }

    var usernameError: MutableLiveData<String> = MutableLiveData()
    var passwordError: MutableLiveData<String> = MutableLiveData()

    var username: ObservableField<String>? = null
    var password: ObservableField<String>? = null

    private lateinit var mContext: Context

    init{
        username = ObservableField("")
        password = ObservableField("")
    }
    fun getContext(context: Context){
        mContext = context
    }

    fun onLoginClick() {
        login()
    }
    fun onUserNameChanges(s: CharSequence, start: Int, befor: Int, count: Int) {
        if(username?.get()!!.length >= 0) usernameError.value =""
    }
    fun onPasswordChanges(s: CharSequence, start: Int, befor: Int, count: Int) {
        if(password?.get()!!.length >= 0) passwordError.value =""
    }

    fun login(){
        if(username?.get()!!.isNullOrEmpty()) {
            usernameError.value =  mContext.getString(R.string.str_username_error)
        }else if(password?.get()!!.isNullOrEmpty()){
            usernameError.value =""
            passwordError.value = mContext.getString(R.string.str_password_error)

        }else{
            usernameError.value =""
            passwordError.value =""
            loadingVisibility.value = View.VISIBLE
            mainviewVisibility.value= View.GONE

            //Set Headers
            val headerMap:HashMap<String,String> = HashMap<String,String>()
            headerMap.put("Content-Type","application/json")
            headerMap.put("IMSI","357175048449937")
            headerMap.put("IMEI","510110406068589")

            //Set Login Perameter
            val perameterMap:HashMap<String,String> = HashMap<String,String>()
            perameterMap.put("username",username?.get()!!)
            perameterMap.put("password",password?.get()!!)

            //Call Login Api
            api.Login(headerMap,perameterMap).enqueue(this)
        }
    }

    override fun onCleared() {
        super.onCleared()

    }

    override fun onResponse(call: Call<UserReponse>, response: Response<UserReponse>) {
        //Hide loading
        loadingVisibility.value = View.GONE;
        //Show Login view
        mainviewVisibility.value= View.VISIBLE

        lateinit var userReponse: UserReponse

        userReponse= response.body()!!
        if(userReponse.errorCode=="00") {
            userReponse.user!!.XAcc = response.headers().get("X-Acc")!!;
            Completable.fromAction {
                response.body()?.let { userDao.insertAll(it) }
            }
                    .subscribeOn(Schedulers.io())
                    .subscribe()
            successMessage.value = mContext.getString(R.string.str_login_success)
        }else{
            errorMessage.value = userReponse.errorMessage
        }
    }

    override fun onFailure(call: Call<UserReponse>, t: Throwable) {
        errorMessage.value = mContext.getString(R.string.error)
        loadingVisibility.value = View.GONE
        mainviewVisibility.value= View.VISIBLE
    }
}


