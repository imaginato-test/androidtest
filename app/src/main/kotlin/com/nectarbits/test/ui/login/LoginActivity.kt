package com.nectarbits.test.ui.login

import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.nectarbits.test.R
import com.nectarbits.test.databinding.ActivityLoginBinding
import com.nectarbits.test.injection.ViewModelFactory


class LoginActivity: AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewModel: LoginViewModel
    private var snackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)

        viewModel = ViewModelProviders.of(this, ViewModelFactory(this)).get(LoginViewModel::class.java)
        viewModel.errorMessage.observe(this, Observer {
            errorMessage -> if(errorMessage != null) showError(errorMessage) else hideSnackBar()
        })
        viewModel.successMessage.observe(this, Observer {
            successMessage -> if(successMessage != null) showSuccess(successMessage) else hideSnackBar()
        })
        viewModel.getContext(this)
        binding.viewModel = viewModel
    }

    private fun showError(errorMessage:String){
        snackbar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_INDEFINITE)
        snackbar?.setAction(R.string.retry, viewModel.errorClickListener)
        snackbar?.show()

    }
    private fun showSuccess(successMessage:String){
        Toast.makeText(this, successMessage, Toast.LENGTH_SHORT).show()
        binding.etUserName.setText("")
        binding.etPassword.setText("")
    }

    private fun hideSnackBar(){
        snackbar?.dismiss()
    }
}